Ext.define('app.view.clientes.PanelClientes', {
    extend: 'Ext.Panel',

    xtype: 'panelclientes',

    layout: 'border',

    controller: 'panelclientesctr',

    tbar: [{
        //xtype:'button',
        text: 'Agregar cliente',
        iconCls: 'x-fa fa-plus',
        handler: 'agregarCliente'
    }],

    items: [{
        region: 'center',
        layout: 'fit',
        xtype: 'tabpanel',
        items: [{
            title: 'Listado de clientes',
            xtype: 'gridclientes',
            reference: 'gridlistado',
            listeners: {
                itemclick: 'showClienteDetalle'
            }
        }],

    }, {
        height: 190, //px
        region: 'south',
        bodyPadding: 10,
        reference: 'paneldetalle',
        tpl: '<div><div style="float: left; margin-right:20px;"> <img src="resources/img/avatars/normal_3.png"></div>' +
            '<h2>{pv_dsnombre1_i}</h2>' +

            '<span style="font-size: 16px; color: #5FA2DD;">{pv_dsocupac_i}</span><br>' +
            '</div>'
    }]

});