Ext.define('app.view.clientes.GridClientes', {
    extend: 'Ext.grid.GridPanel',

    xtype: 'gridclientes',

    initComponent: function() {

        var mistore = Ext.create('app.store.usuarios.StoreUsuarios', {
            autoLoad: true
        });

        Ext.apply(this, {

            store: mistore,

            columns: [{
                    text: 'Nombre',
                    dataIndex: 'pv_dsnombre1_i',
                    flex: 1,
                    renderer: function(val, meta, rec) {
                        return rec.nombreToHTML();
                    }
                },

                {
                    text: 'Fecha nacimiento',
                    dataIndex: 'pv_fecnacim_i',
                    width: 600,
                    menuDisabled: true,
                    renderer: Ext.util.Format.dateRenderer('d-m-Y')
                },
                {
                    text: 'Ocupación',
                    dataIndex: 'pv_dsocupac_i',
                    width: 450,
                    menuDisabled: true,


                }

            ],

            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: mistore,
                dock: 'bottom',
                displayInfo: true
            }]

        });

        this.callParent();
    }

});