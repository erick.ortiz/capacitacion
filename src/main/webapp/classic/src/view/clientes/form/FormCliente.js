Ext.define('app.view.clientes.form.FormCliente', {
    extend: 'Ext.form.Panel',

    xtype: 'formcliente',

    bodyPadding: 10,

    //layout:'anchor' by default

    initComponent: function() {

        Ext.apply(this, {
            fieldDefaults: {
                labelAlign: 'right', //alineacion a la derecha
                labelWidth: 110, // mas ancho en la etiqueta
                msgTarget: 'under', //mensaje de error por debajo
                anchor: '100%', //100% de ancho
                allowBlank: false //obligatorio
            },

            items: [{
                fieldLabel: '* Nombre1',
                emptyText: 'Campo obligatorio',
                xtype: 'textfield',
                reference: 'pv_dsnombre1_i',
                name: 'pv_dsnombre1_i'
            }, {
                fieldLabel: '* Nombre2',
                emptyText: 'Campo obligatorio',
                xtype: 'textfield',
                allowBlank: true,
                reference: 'pv_dsnombre2_i',
                name: 'pv_dsnombre2_i'
            }, {
                fieldLabel: '* Apellido paterno',
                emptyText: 'campo obligatorio',
                xtype: 'textfield',
                reference: 'pv_dsapepat_i',
                name: 'pv_dsapepat_i'
            }, {
                fieldLabel: '* Apellido materno',
                emptyText: 'campo obligatorio',
                xtype: 'textfield',
                reference: 'pv_dsapemat_i',
                name: 'pv_dsapemat_i'
            }, {
                fieldLabel: 'Fecha de nacimiento',
                xtype: 'datefield',
                maxValue: new Date(),
                format: "d-m-Y",
                allowBlank: true,
                reference: 'pv_fecnacim_i',
                name: 'pv_fecnacim_i'
            }, {
                fieldLabel: 'Ocupación',
                xtype: 'textfield',
                reference: 'pv_dsocupac_i',
                name: 'pv_dsocupac_i'
            }, {
                xtype: 'hiddenfield',
                fieldLabel: '* Id',
                emptyText: 'Campo opcional',
                reference: 'pv_cdusuario_i',
                name: 'pv_cdusuario_i',
                value: "0"
            }, {
                xtype: 'hiddenfield',
                fieldLabel: '* OPERACION',
                emptyText: 'Campo obligatorio(I U D)',
                reference: 'pv_operacion_i',
                name: 'pv_operacion_i',
                value: "I"
            }]


        });



        this.callParent();


    },

    doSubmit: function() {
        this.getForm().submit({
            url: 'server/doformpost.json',
            success: function(form, result) {
                console.info(result);
            },
            failure: function(form, result) {
                console.info(result);
            }
        });
    },

    buttons: [{
            text: 'Guardar',
            handler: function() {
                var form = this.up('form');
                if (form.isValid()) {
                    var form = this.up('form').getValues();

                    console.log('values', form);

                    Ext.Ajax.request({
                        url: 'http://localhost:8080/api/usuariosjson',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8'
                        },
                        params: Ext.JSON.encode(form),
                        success: function(form, result) {
                            console.info('Ejecutado con exito');
                            Ext.Msg.alert('Proceso exitoso', 'Se creo un usuario exitosamente');

                        },
                        failure: function(form, result) {
                            console.info('Error');
                        }
                    });

                } else {
                    Ext.Msg.alert('Formulario Invalido', 'Porfavor, Ingrese sus datos nuevamente.');
                }
            },
        }

    ],

});