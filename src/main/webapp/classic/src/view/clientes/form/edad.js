Ext.define('app.view.clientes.form.edad', {
    extend: 'Ext.form.Panel',
    xtype: 'panelClientes',
    layout: 'border',

    tbar: [{
        text: 'calcular edad',
        iconCls: 'x-fa fa-plus',
        handler: function() {
            var window = Ext.create('Ext.Window', {
                modal: true,
                title: 'Edad',
                heigth: 300,
                width: 400,
                layout: 'fit',
                items: [{
                    xtype: 'formcliente'
                }],
                buttons: [{
                    text: 'submit',
                    handler: function Edad(FechaNacimiento) {

                        var fechaNace = new Date(FechaNacimiento);
                        var fechaActual = new Date()

                        var mes = fechaActual.getMonth();
                        var dia = fechaActual.getDate();
                        var ano = fechaActual.getFullYear();

                        fechaActual.setDate(dia);
                        fechaActual.setMonth(mes);
                        fechaActual.setFullYear(ano);

                        edad = Math.floor(((fechaActual - fechaNace) / (1000 * 60 * 60 * 24) / 365));

                        return edad;
                    }
                }]
            });
        }
    }]
});