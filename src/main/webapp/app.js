/*
 * This file is generated and updated by Sencha Cmd. You can edit this file as
 * needed for your application, but these edits will have to be merged by
 * Sencha Cmd when upgrading.
 */
Ext.application({
    name: 'app',

    extend: 'app.Application',

    requires: [
        'app.view.common.WindowForm',
        'app.view.common.ModalWindow',
        'app.view.main.Main',
        'app.view.clientes.PanelClientesController',
        'app.view.clientes.PanelClientes',
        'app.view.clientes.form.FormCliente',
        'app.view.clientes.GridClientes',
        'app.view.clientes.ClientesController',
        "app.view.clientes.form.edad",
        'app.model.usuarios.usuarioModel',
        'app.store.usuarios.StoreUsuarios'
    ],

    // The name of the initial view to create. With the classic toolkit this class
    // will gain a "viewport" plugin if it does not extend Ext.Viewport. With the
    // modern toolkit, the main view will be added to the Viewport.
    //
    mainView: 'app.view.main.Main'

    //-------------------------------------------------------------------------
    // Most customizations should be made to app.Application. If you need to
    // customize this file, doing so below this section reduces the likelihood
    // of merge conflicts when upgrading to new versions of Sencha Cmd.
    //-------------------------------------------------------------------------
});