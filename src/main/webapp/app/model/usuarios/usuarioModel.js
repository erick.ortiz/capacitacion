Ext.define('app.model.usuarios.usuarioModel', {
    extend: 'Ext.data.Model',
    fields: [
        'pv_dsnombre1_i',
        'pv_dsnombre2_i',
        'pv_dsapepat_i',
        'pv_dsapemat_i',
        { name: "pv_fecnacim_i", type: 'date', format: 'd-m-Y' },
        'pv_dsocupac_i',
        { name: 'pv_cdusuario_i', type: 'int' }

    ],

    nombreToHTML: function() {
        return this.data.pv_dsapepat_i + ' ' + this.data.pv_dsapemat_i +
            ', <b>' + this.get('pv_dsnombre1_i') + ' ' + this.get('pv_dsnombre2_i') + '</b>';
    }

});