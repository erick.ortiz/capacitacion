Ext.define('app.view.clientes.PanelClientesController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.panelclientesctr',

    showClienteDetalle: function(grid, rec) {
        console.info(rec);

        var panel = this.lookupReference('paneldetalle');
        panel.update(rec.data);
    },

    agregarCliente: function() {
        var window = Ext.create('app.view.common.WindowForm', {
            //modal: true,
            title: 'Agregar un nuevo Usuario',
            height: 490,
            width: 400,
            //layout:'fit',
            form: Ext.create('app.view.clientes.form.FormCliente')
        });

        window.show();
    }

});