Ext.define('app.store.usuarios.StoreUsuarios', {
    extend: 'Ext.data.Store',

    model: 'app.model.usuarios.usuarioModel',

    proxy: {
        type: 'ajax',
        url: 'http://localhost:8080/api/usuariosjson',

        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    }
});