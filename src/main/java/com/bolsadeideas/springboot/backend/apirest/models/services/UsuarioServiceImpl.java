package com.bolsadeideas.springboot.backend.apirest.models.services;


import java.util.List;

import javax.persistence.EntityManager;

import javax.persistence.ParameterMode;

import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.backend.apirest.models.dao.IUsuarioDao;
import com.bolsadeideas.springboot.backend.apirest.models.entity.UsuarioDAOImpl;
import com.bolsadeideas.springboot.backend.apirest.models.entity.UsuarioDAOImpl_CURSOR;


@Service
public class UsuarioServiceImpl implements IUsuarioDao {
	
	
	 
	protected final Log logger = LogFactory.getLog(this.getClass());
	
	@PersistenceContext
    private EntityManager entity;
	
	@Override
	public boolean save(UsuarioDAOImpl usuario) {
	try {
			
			StoredProcedureQuery storedProcedure = entity.createStoredProcedureQuery("JAVIER.PKG_DML.CRUD_USUARIOS",UsuarioDAOImpl.class);
		
			 storedProcedure
			.registerStoredProcedureParameter(0, String.class, ParameterMode.IN)
			.registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
			.registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
			.registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
			.registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
			.registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
			.registerStoredProcedureParameter(6, Integer.class, ParameterMode.IN)
		    .registerStoredProcedureParameter(7, String.class, ParameterMode.IN);
			

			  storedProcedure
			  .setParameter(0,usuario.getPv_dsnombre1_i())
			  .setParameter(1,usuario.getPv_dsnombre2_i())
			  .setParameter(2,usuario.getPv_dsapepat_i())
			  .setParameter(3,usuario.getPv_dsapemat_i())
			  .setParameter(4,usuario.getPv_fecnacim_i())
			  .setParameter(5,usuario.getPv_dsocupac_i())
			  .setParameter(6,usuario.getPv_cdusuario_i())
			  .setParameter(7,usuario.getPv_operacion_i()); //pv_operacion_i
			
			storedProcedure.execute();
			
			
		} catch (Exception e) {
			logger.info("Imp. algo paso!!!");
			e.printStackTrace();
			
			return false;
		}
	
		return true;
		
	    }

	

	@SuppressWarnings("unchecked")
	@Override
	public List<UsuarioDAOImpl_CURSOR> getUsers() {
	     
		StoredProcedureQuery procedureQuery = entity.createStoredProcedureQuery("JAVIER.PKG_DML.Registros_sistema", UsuarioDAOImpl_CURSOR.class);
	      
	      procedureQuery.registerStoredProcedureParameter("pCursor", void.class, ParameterMode.REF_CURSOR);
	      
	      procedureQuery.execute();
	      
	      List<UsuarioDAOImpl_CURSOR> resultList = procedureQuery.getResultList();
		
	      return resultList;
	
	}







			
}


