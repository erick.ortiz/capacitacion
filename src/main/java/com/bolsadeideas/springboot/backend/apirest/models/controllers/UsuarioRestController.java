package com.bolsadeideas.springboot.backend.apirest.models.controllers;



import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bolsadeideas.springboot.backend.apirest.models.entity.UsuarioDAOImpl;
import com.bolsadeideas.springboot.backend.apirest.models.entity.UsuarioDAOImpl_CURSOR;
import com.bolsadeideas.springboot.backend.apirest.models.services.UsuarioServiceImpl;



@RestController
@CrossOrigin
@RequestMapping("/api")
public class UsuarioRestController {
	
	 Logger logger = LoggerFactory.getLogger(UsuarioRestController.class);

	@Autowired
	private UsuarioServiceImpl usuarioImpl;
	
	@RequestMapping(value="/usuariosjson",method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public boolean save(@RequestBody UsuarioDAOImpl usuario) {
		
		
		
		logger.info(" ----POST MAPPING------ ");
	
		logger.info(
		"USUARIO CAPTURADO"         + " " + 
		usuario.getPv_dsnombre1_i() + " " + 
		usuario.getPv_dsnombre2_i() + " " + 
		usuario.getPv_dsapepat_i()  + " " +
		usuario.getPv_dsapemat_i()  + " " + 
		usuario.getPv_fecnacim_i()  + " " + 
		usuario.getPv_dsocupac_i()  + " " + 
		usuario.getPv_operacion_i() + " " + //pv_operacion_i
		usuario.getPv_cdusuario_i());
	
		
	logger.info(" ----USUARIO GUARDADO------ ");	
	
		return usuarioImpl.save(usuario);
	}
	
	
	@RequestMapping(value="/usuariosjson",method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.CREATED)
	public List<UsuarioDAOImpl_CURSOR> getUsers(){
		
		logger.info(" ----GET MAPPING------ ");
		
		logger.info(" ----LISTANDO USUARIOS------ ");
		
		return usuarioImpl.getUsers();
	}
	
	
	
	
}
	
