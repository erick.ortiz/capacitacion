package com.bolsadeideas.springboot.backend.apirest.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;

import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;


@Entity
@Table(name="USUARIOS")
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name="Usuario_Crud_Usuarios",
                procedureName="JAVIER.PKG_DML.CRUD_USUARIOS",
                resultClasses = { UsuarioDAOImpl.class },
                parameters={
                    @StoredProcedureParameter(name="pv_dsnombre1_i", type=String.class, mode=ParameterMode.IN),
                    @StoredProcedureParameter(name="pv_dsnombre2_i", type=String.class, mode=ParameterMode.IN),
                    @StoredProcedureParameter(name="pv_dsapepat_i", type=String.class, mode=ParameterMode.IN),
                    @StoredProcedureParameter(name="pv_dsapemat_i", type=String.class, mode=ParameterMode.IN),
                    @StoredProcedureParameter(name="pv_fecnacim_i", type=String.class, mode=ParameterMode.IN),
                    @StoredProcedureParameter(name="pv_dsocupac_i", type=String.class, mode=ParameterMode.IN),
                	@StoredProcedureParameter(name="pv_cdusuario_i", type=Integer.class, mode=ParameterMode.IN),
                	@StoredProcedureParameter(name="pv_operacion_i", type=String.class, mode=ParameterMode.IN)
                    
                })
       

})



public class UsuarioDAOImpl implements Serializable{
	
	@Id
	@GeneratedValue
	@Column(name = "ID_USUARIO") 
	private Integer pv_cdusuario_i;
	
	
	@Column(name = "NOMBRE") 
	private String pv_dsnombre1_i;
	
	
	@Column(name = "NOMBRE2") 
	private String pv_dsnombre2_i;
	
	@Column(name = "APEPAT") 
	private String pv_dsapepat_i;
	
	@Column(name = "APEMAT") 
	private String pv_dsapemat_i;
	
	@Column(name = "FECNAC") 
	private String pv_fecnacim_i;
	
	
	@Column(name = "OCUPACION") 
	private String pv_dsocupac_i;	
	
	
	private String pv_operacion_i;

	
	
	


	public Integer getPv_cdusuario_i() {
		return pv_cdusuario_i;
	}

	public void setPv_cdusuario_i(Integer pv_cdusuario_i) {
		this.pv_cdusuario_i = pv_cdusuario_i;
	}

	public String getPv_dsnombre1_i() {
		return pv_dsnombre1_i;
	}

	public void setPv_dsnombre1_i(String pv_dsnombre1_i) {
		this.pv_dsnombre1_i = pv_dsnombre1_i;
	}

	public String getPv_dsnombre2_i() {
		return pv_dsnombre2_i;
	}

	public void setPv_dsnombre2_i(String pv_dsnombre2_i) {
		this.pv_dsnombre2_i = pv_dsnombre2_i;
	}

	public String getPv_dsapepat_i() {
		return pv_dsapepat_i;
	}

	public void setPv_dsapepat_i(String pv_dsapepat_i) {
		this.pv_dsapepat_i = pv_dsapepat_i;
	}

	public String getPv_dsapemat_i() {
		return pv_dsapemat_i;
	}

	public void setPv_dsapemat_i(String pv_dsapemat_i) {
		this.pv_dsapemat_i = pv_dsapemat_i;
	}

	public String getPv_fecnacim_i() {
		return pv_fecnacim_i;
	}

	public void setPv_fecnacim_i(String pv_fecnacim_i) {
		this.pv_fecnacim_i = pv_fecnacim_i;
	}

	public String getPv_dsocupac_i() {
		return pv_dsocupac_i;
	}

	public void setPv_dsocupac_i(String pv_dsocupac_i) {
		this.pv_dsocupac_i = pv_dsocupac_i;
	}



	public String getPv_operacion_i() {
		return pv_operacion_i;
	}

	public void setPv_operacion_i(String pv_operacion_i) {
		this.pv_operacion_i = pv_operacion_i;
	}



	private static final long serialVersionUID = 1L;


}
